"""
	Messenger Plus sound server implementation
	
	GET /esnd/snd/builtin?code={code} -> builtin
	GET /esnd/snd/check?hash={hash} -> check
	GET /esnd/snd/get?hash={hash} -> get
	GET /esnd/snd/random?[catId={category}]&[lngId={language}] -> random
	POST /esnd/snd/put[?uf=1] -> put
"""

from aiohttp import web
from pathlib import Path
from collections import namedtuple
from random import randrange

from db import Sound, Session

def create_app(*, serve_static = False):
	app = web.Application()
	
	app.router.add_get('/esnd/snd/builtin', builtin)
	app.router.add_get('/esnd/snd/check', check)
	app.router.add_get('/esnd/snd/get', get)
	app.router.add_get('/esnd/snd/random', random)
	app.router.add_post('/esnd/snd/put', put)
	
	return app

SOUND_DIR = Path('sound')
PATH_BUILTINS = SOUND_DIR / 'builtins'
PATH_USERS = SOUND_DIR / 'users'

Metadata = namedtuple('Metadata', ['title', 'hash', 'category', 'language', 'is_public'])

async def builtin(req):
	"""
		Get builtin sound file
		
		(GET) /esnd/snd/builtin?code={code}
		
		:return file content or 0 if file is not found
	"""
	file_name = req.query['code'] + '.mp3'
	file_path = PATH_BUILTINS / file_name
	
	try:
		return web.HTTPOk(content_type = 'audio/mpeg', body = file_path.read_bytes())
	except FileNotFoundError:
		return web.HTTPOk(text = str(0))

async def check(req):
	"""
		Check if sound file is available
		
		GET /esnd/snd/check?hash={hash}
		
		:return 1 if file exists 0 otherwise
	"""
	file_hash = req.query['hash']
	
	result = int(_get_file_path(file_hash).exists())
	
	return web.HTTPOk(text = str(result))

async def put(req):
	"""
		Upload new sound file.
		Overwrite existing file if uf=1
		
		POST /esnd/snd/put?[uf=1]
		
		:return 1 for success 0 for failure
	"""
	data = await req.post()
	file = data['file']
	if not isinstance(file, web.FileField):
		return web.HTTPOk(text = str(0))
	
	with file.file as f:
		f.seek(-128, 2) # metadata offset
		metadata = _parse_metadata(f.read())
		
		file_path = _get_file_path(metadata.hash)
		file_path.parent.mkdir(parents = True)
		
		try:
			output = file_path.open('xb')
		except FileExistsError:
			if req.rel_url.query.get('uf') != 1:
				return web.HTTPOk(text = str(0))
			output = file_path.open('wb')
		
		f.seek(0)
		with output as o:
			o.write(f.read())
	
	with Session() as session:
		session.merge(Sound(**metadata._asdict()))
	
	return web.HTTPOk(text = str(1))

async def get(req):
	"""
		Get sound file
		
		GET /esnd/snd/get?hash={hash}
		
		:return file content or 0 if file is not found
	"""
	file_hash = req.query['hash']
	file_path = _get_file_path(file_hash)
	
	try:
		return web.HTTPOk(content_type = 'audio/mpeg', body = file_path.read_bytes())
	except FileNotFoundError:
		return web.HTTPOk(text = str(0))

async def random(req):
	"""
		Get random sound from library
		
		GET /esnd/snd/random?[catId={category}]&[lngId={language}]
		
		:return: file content or 0 if file is not found
	"""
	category = req.query.get('catId')
	language = req.query.get('lngId')
	
	with Session() as session:
		query = session.query(Sound).filter(Sound.is_public == True)
		
		if category:
			query = query.filter(Sound.category == category)
		
		if language:
			query = query.filter(Sound.language == language)
		
		try:
			offset = randrange(0, query.count())
		except ValueError:
			return web.HTTPOk(text = str(0))
		
		sound = query.offset(offset).limit(1).one()
		
		file_path = _get_file_path(sound.hash)
		
		try:
			return web.HTTPOk(content_type = 'audio/mpeg', body = file_path.read_bytes())
		except FileNotFoundError:
			return web.HTTPOk(text = str(0))

def _get_file_path(file_hash):
	"""
		Get file path by hash.
		Uses first 3 symbols for subdirectories e.g. 12345 -> 1/2/3/12345.mp3
		
		:type file_hash: string
		:rtype: string
	"""
	file_dir = PATH_USERS / file_hash[0] / file_hash[1] / file_hash[2]
	return file_dir / (file_hash + '.mp3')

def _parse_metadata(raw_data):
	"""
		Parse raw metadata
		
		00:32   - TAG{title}0x00 ... 0x00
		93      - {category}
		95      - {is_public} - 0x02 = true
		109:120 - {hash}
		127     - {language}
		
		:param raw_data: 128 bytes
		:rtype: Metadata
	"""
	
	return Metadata(
		title = raw_data[3:33].decode('utf-8').strip('\0'),
		category = raw_data[93],
		is_public = (raw_data[95] in (0, 2)),
		hash = raw_data[109:121].decode('utf-8'),
		language = raw_data[127],
	)
